package br.com.cowtysys.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.cowtysys.exceptions.CryptException;

public abstract class CryptHelper {
	private static final Logger log = LoggerFactory.getLogger(CryptHelper.class);

	
	public static final String MD2 = "MD2";
	public static final String MD5 = "MD5";
	public static final String SHA = "SHA";
	public static final String SHA256 = "SHA-256";
	public static final String SHA384 = "SHA-384";
	public static final String SHA512 = "SHA-512";
	
	public static String getCryptedString(String text, String algorithm) throws CryptException{
		try{
			MessageDigest m;
			m = MessageDigest.getInstance(algorithm);
			m.update(text.getBytes(), 0, text.length());
			String cryptText = new BigInteger(1,m.digest()).toString(16);
			
			return cryptText;
        }catch(NoSuchAlgorithmException e){
        	log.error("function=getCryptedString() message=[Algorithm not found] exceptionMessage=[" + e.getMessage() + "]");
        	
            throw new CryptException("Could not encrypt the text: " + e.getMessage());
        }catch(NullPointerException e){
        	log.error("function=getCryptedString() message=[Invalid Object]");
        	
            throw new CryptException("Could not encrypt the text: Invalid Object");
        }

	}
}
