package br.com.cowtysys.util;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.core.InvalidReferenceException;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class TemplateHelper {
	private static final Logger log = LoggerFactory.getLogger(TemplateHelper.class);
	
	/**
	 * Processa os dados de acordo com um template informado
	 * 
	 * @author Efraim Ferreira
	 * @since 15-07-2014
	 * @return Representation
	 * @param data Map[String, Object] dados a serem processados no Template
	 * @param templateName String nome do template
	 * @param mediaType MediaType Media-Type da resposta ao cliente
	 * @return
	 * @throws InvalidReferenceException
	 * @throws IOException
	 * @throws TemplateException
	 */
	public static Representation loadTemplate(Map<String, Object> data, String templateName, MediaType mediaType) throws InvalidReferenceException, IOException, TemplateException{
		log.trace("function=loadTemplate() status=init");
		templateName = templateName + ".ftl";
		
		Configuration config = new Configuration();
		config.setClassForTemplateLoading(TemplateHelper.class, "../templates/");
		
		StringWriter wr = new StringWriter();

		Template temp = config.getTemplate(templateName);
		temp.process(data, wr);
		
		log.debug("template=" + templateName + " message=[Template rendered successfully]");
		log.trace("function=loadTemplate() status=done");
		
		return new StringRepresentation(wr.toString(), mediaType);
	}
}
