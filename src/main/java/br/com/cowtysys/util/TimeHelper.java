package br.com.cowtysys.util;

import java.util.Date;

public class TimeHelper {
	
	/**
	 * Returns the current timestamp
	 * 
	 * @author Efraim Ferreira
	 * @since 31-05-2014
	 * @return long
	 */
	public static long getTimestamp(){
		return getCurrentTimestamp();
	}
	
	/**
	 * Returns the difference between a given timestamp and the current timestamp
	 * 
	 * @param long initTime
	 * @return long
	 */
	public static long getTotalProcessTime(long initTime){
		return getCurrentTimestamp() - initTime;
	}
	
	protected static long getCurrentTimestamp(){
		return new Date().getTime();
	}
}
