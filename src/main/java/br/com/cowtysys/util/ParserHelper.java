package br.com.cowtysys.util;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import org.codehaus.jackson.map.ObjectMapper;
import org.restlet.data.Form;
import org.restlet.data.Parameter;
import org.restlet.representation.Representation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.cowtysys.auth.SecurityAuthenticator;
import br.com.cowtysys.exceptions.CastException;

public class ParserHelper {

	private static final Logger log = LoggerFactory.getLogger(SecurityAuthenticator.class);
	
	/**
	 * 
	 * @param entity
	 * @param type
	 * @return
	 * @throws IOException 
	 * @throws CastException 
	 */
	public static <T> T castEntityToObject(Representation entity, Class<T> type) throws CastException {
		log.trace("function=castToObject() status=init");
		
		T object = null;
		try {
			log.debug(entity.getText());
			Map<String, Object> map = new TreeMap<String, Object>();
			Form form = new Form(entity);
			for(Parameter parameter : form){
				map.put(parameter.getName(), parameter.getValue());
				log.debug("Parameter: " + parameter);
			}
			object = new ObjectMapper().readValue(new ObjectMapper().writer().writeValueAsString(map), type);
		} catch (IOException e) {
			log.error("message=[Error on parser request] exceptionMessage=[" + e.getMessage() + "]");
			throw new CastException("Invalid request data.");
		}
		log.trace("function=castToObject() status=done");
		
		return object;
	}
	
	/**
	 * 
	 * @param json
	 * @param type
	 * @return
	 * @throws CastException 
	 */
	public static <T> T castJsonToObject(String json, Class<T> type) throws CastException {
		log.trace("function=castToObject() status=init");
		
		T object = null;
		try {
			object = new ObjectMapper().readValue(json, type);
		} catch (IOException e) {
			log.error("message=[Error on parser request] exceptionMessage=[" + e.getMessage() + "]");
			throw new CastException("Invalid request data.");
		}
		log.trace("function=castToObject() status=done");
		
		return object;
	}
}
