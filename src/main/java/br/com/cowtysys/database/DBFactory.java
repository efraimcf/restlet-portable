package br.com.cowtysys.database;

import br.com.cowtysys.database.mongodb.MongoDB;

/**
 * Factory de criação de objetos de conexão com bancos de dados usando Spring Data
 * 
 * @author Efraim
 *
 */
public abstract class DBFactory {

	public static MongoDB getMongoDBConnection(){
		return new MongoDB();
	}
}
