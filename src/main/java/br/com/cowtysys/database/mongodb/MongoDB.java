package br.com.cowtysys.database.mongodb;

import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.BasicQuery;

import br.com.cowtysys.models.Entity;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * Factory de criação de banco de dados com o MongoDB
 * 
 * @author Efraim
 *
 */
public class MongoDB {
	protected MongoOperations mongoOperation;
	protected ApplicationContext mongoContext;

	public MongoDB(){
		this.mongoContext = new AnnotationConfigApplicationContext(SpringMongoConfig.class);
		this.mongoOperation = (MongoOperations) this.mongoContext.getBean("mongoTemplate");
	}
	
	public void save(Entity entity){
		mongoOperation.save(entity);
	}
	
	public <T> List<T> findAll(Class<T> type){
		return mongoOperation.findAll(type);
	}
	
	public <T> T findOne(Map<String, Object> queryData, Class<T> type){
		DBObject doc = new BasicDBObject(queryData);
		BasicQuery query = new BasicQuery(doc);

		return mongoOperation.findOne(query, type);
	}

}
