package br.com.cowtysys.domain;


public class ServiceRequest<T> {
	
	private String accessKey;
	
	private T info;

	public String getAccessKey() {
		return accessKey;
	}
	
	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}
	
	public T getInfo() {
		return info;
	}

	public void setInfo(T info) {
		this.info = info;
	}
	
}
