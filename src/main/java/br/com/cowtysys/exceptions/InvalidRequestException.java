package br.com.cowtysys.exceptions;

public class InvalidRequestException extends Exception{

	private static final long serialVersionUID = 5753765411304866405L;

	public InvalidRequestException() {
	}

	public InvalidRequestException(String message) {
		super(message);
	}

	public InvalidRequestException(Throwable cause) {
		super(cause);
	}

	public InvalidRequestException(String message, Throwable cause) {
		super(message, cause);
	}

}
