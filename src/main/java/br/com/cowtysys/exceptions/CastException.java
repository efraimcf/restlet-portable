package br.com.cowtysys.exceptions;

public class CastException extends Exception {

	private static final long serialVersionUID = -8487182627990776722L;

	public CastException() {
	}

	public CastException(String message) {
		super(message);
	}

	public CastException(Throwable cause) {
		super(cause);
	}

	public CastException(String message, Throwable cause) {
		super(message, cause);
	}

}
