package br.com.cowtysys.auth;

import java.util.HashMap;
import java.util.Map;

import org.restlet.Context;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.Form;
import org.restlet.data.Method;
import org.restlet.data.Status;
import org.restlet.engine.header.Header;
import org.restlet.security.Authenticator;
import org.restlet.util.NamedValue;
import org.restlet.util.Series;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.cowtysys.database.DBFactory;
import br.com.cowtysys.database.mongodb.MongoDB;
import br.com.cowtysys.domain.ServiceRequest;
import br.com.cowtysys.exceptions.CastException;
import br.com.cowtysys.exceptions.InvalidRequestException;
import br.com.cowtysys.models.User;
import br.com.cowtysys.util.ParserHelper;

public class SecurityAuthenticator extends Authenticator {

	private static final Logger log = LoggerFactory.getLogger(SecurityAuthenticator.class);
	private static final String HEADERS_ATTRIBUTE = "org.restlet.http.headers";
	private static final String SECURITY_KEY = "secureityKey";
	private static final String ACCESS_KEY = "accessKey";
	
	private MongoDB db;
	
	public SecurityAuthenticator(Context context) {
		super(context);
		db = DBFactory.getMongoDBConnection();
	}

	@Override
	protected boolean authenticate(Request request, Response response) {
		log.trace("function=authenticate() status=init");
		boolean valid = false;
		
		//TODO: BlackList And GrayList
		try {
			String securityKey = setSecurityToken(request);
			String accessKey = setAccessToken(request);
			String clientIp = getClientIp(request);
			String requestData = getRequestData(request);
			valid = validate(accessKey, securityKey, requestData, clientIp);
		} catch (Exception e) {
			log.error("message=[Error validating request] exceptionMessage=[" + e.getMessage() + "]");
			response.setStatus(Status.SERVER_ERROR_INTERNAL);

			Series<Header> responseHeaders = new Series<Header>(Header.class);
			responseHeaders.add(new Header("accessToken", "newAccessToken"));
		    response.getAttributes().put(HEADERS_ATTRIBUTE, responseHeaders);
			
			valid = false;
		}
		log.trace("function=authenticate() status=done");
		
		return valid;
	}

	@Override
	protected int unauthenticated(Request request, Response response) {
		return STOP;
	}

	
	@SuppressWarnings("unchecked")
	private String setSecurityToken(Request request) throws InvalidRequestException {
		Series<NamedValue<String>> headers = (Series<NamedValue<String>>) request.getAttributes().get(HEADERS_ATTRIBUTE);
		String securityKey = headers.getValues(SECURITY_KEY);
		if (securityKey == null) throw new InvalidRequestException("Invalid security key");
		log.debug("message=[Authenticating Security Key: " + securityKey + "]");
		
		return securityKey;
	}
	
	@SuppressWarnings("unchecked")
	private String setAccessToken(Request request) throws CastException, InvalidRequestException {
		String accessKey = null;
		if (request.getMethod() == Method.GET){
			Form form = request.getResourceRef().getQueryAsForm();
			accessKey = form.getValues(ACCESS_KEY);
		}else if (request.getMethod() == Method.POST){
			ServiceRequest<Object> sr = ParserHelper.castJsonToObject(request.getEntityAsText(), ServiceRequest.class); 
			accessKey = sr.getAccessKey();
		}
		if (accessKey == null) throw new InvalidRequestException("Invalid access key");
		log.debug("message=[Authenticating Access Key: " + accessKey + "]");
		
		return accessKey;
	}
	
	private String getClientIp(Request request) {
		String clientIp = request.getClientInfo().getAddress();
		return clientIp;
	}

	private String getRequestData(Request request) {
		return request.getEntityAsText();
	}
	
	public boolean validate(String accessKey, String securityKey, String requestData, String clientIp) {
		Map<String, Object> queryData = new HashMap<String, Object>();
		queryData.put(ACCESS_KEY, accessKey);
		
		User user = db.findOne(queryData, User.class);
		if (user != null){
			log.debug("message=[Validating user: " + user.getUsername() +"]");
			//TODO: Validate securityKey
		}else{
			log.warn("message=[Request unauthorized from " + clientIp + "]");
		}
		
		return true;
	}
}
