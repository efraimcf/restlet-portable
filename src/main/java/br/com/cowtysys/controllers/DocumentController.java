package br.com.cowtysys.controllers;

import org.restlet.Request;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.Post;

public class DocumentController extends BaseController {
	
	@Post
	@Override
	public Representation doPostRequest(Representation entity){
		log.trace("function=postRequest status=init");
		
		Request req = getRequest();
		
		String text = 	"Request URI: " + req.getOriginalRef()
						+ "\nMethod: " + req.getMethod()
						+ "\nIP Address: " + getClientInfo().getAddress()
						+ "\nAgent Name: " + getClientInfo().getAgentName()
						+ "\nAgent Version: " + getClientInfo().getAgentVersion()
						+ "\n";
		StringRepresentation resp = new StringRepresentation(text);

		log.trace("function=postRequest status=done");
		
		return resp;
	}
	
	@Get
	@Override
	public Representation doGetRequest(Representation entity){
		log.trace("function=getRequest status=init");
		
		Request req = getRequest();
		
		String text = 	"Request URI: " + req.getOriginalRef()
						+ "\nMethod: " + req.getMethod()
						+ "\nIP Address: " + getClientInfo().getAddress()
						+ "\nAgent Name: " + getClientInfo().getAgentName()
						+ "\nAgent Version: " + getClientInfo().getAgentVersion()
						+ "\n";
		StringRepresentation resp = new StringRepresentation(text);
		
		log.trace("function=getRequest status=done");
		
		return resp;
	}
}