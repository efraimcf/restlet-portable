package br.com.cowtysys.controllers;

import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Patch;
import org.restlet.resource.Post;
import org.restlet.resource.Put;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.cowtysys.util.TimeHelper;

public abstract class BaseController extends ServerResource{
	
	protected Logger log;
	protected long initRequest;
	protected String controllerName;
	
	public BaseController(){
		super();
		String totalName = this.getClass().getName();
		String[] data = totalName.split("\\.");
		controllerName = data[data.length - 1];
		log = LoggerFactory.getLogger(this.getClass());
	}
	
	/**
	 * Init process to all Controllers
	 */
	@Override
	protected void doInit() throws ResourceException {
		log.trace("function=doInit() status=init");
		initRequest = TimeHelper.getTimestamp();
		log.trace("function=doInit() status=done");
	}

	/**
	 * Release process to all Controllers 
	 */
	@Override
	protected void doRelease() throws ResourceException {
		log.trace("function=doRelease() status=init");
		long totalTime = TimeHelper.getTotalProcessTime(initRequest);
		//TODO: Generate Statics
		log.debug("IP=" + getClientInfo().getAddress() + " processTime=" + totalTime + "ms");
		
		log.trace("function=doRelease() status=done");
	}

	/**
	 * Post Method - Creates a new entity
	 * 
	 * @param entity
	 * @return
	 */
	@Post
	public Representation doPostRequest(Representation entity) {
		log.trace("function=doPostRequest() status=init");
		setStatus(Status.SERVER_ERROR_NOT_IMPLEMENTED);
		log.trace("function=doPostRequest() status=done");
		
		return null;
	}
	
	/**
	 * Get Method - Retrieves an entity
	 * 
	 * @param entity
	 * @return
	 */
	@Get
	public Representation doGetRequest(Representation entity) {
		log.trace("function=doGetRequest() status=init");
		setStatus(Status.SERVER_ERROR_NOT_IMPLEMENTED);
		log.trace("function=doGetRequest() status=done");
		
		return null;
	}

	/**
	 * Put Method - Replaces an existent entity
	 * 
	 * @param entity
	 * @return
	 */
	@Put
	public Representation doPutRequest(Representation entity) {
		log.trace("function=doPutRequest() status=init");
		setStatus(Status.SERVER_ERROR_NOT_IMPLEMENTED);
		log.trace("function=doPutRequest() status=done");
		
		return null;
	}

	/**
	 * Patch Method - Update an existent entity
	 * 
	 * @param entity
	 * @return
	 */
	@Patch
	public Representation doPatchRequest(Representation entity) {
		log.trace("function=doPatchRequest() status=init");
		setStatus(Status.SERVER_ERROR_NOT_IMPLEMENTED);
		log.trace("function=doPatchRequest() status=done");
		
		return null;
	}

	/**
	 * Delete Method - Delete an existent entity
	 * 
	 * @param entity
	 * @return
	 */
	@Delete
	public Representation doDeleteRequest(Representation entity) {
		log.trace("function=doDeleteRequest() status=init");
		setStatus(Status.SERVER_ERROR_NOT_IMPLEMENTED);
		log.trace("function=doDeleteRequest() status=done");
		
		return null;
	}
}
