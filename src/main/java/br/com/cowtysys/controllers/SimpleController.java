package br.com.cowtysys.controllers;

import org.restlet.Request;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Patch;
import org.restlet.resource.Post;
import org.restlet.resource.Put;

public class SimpleController extends BaseController {
	
	@Post
	@Override
	public Representation doPostRequest(Representation entity){
		log.trace("function=doPostRequest status=init");

		Request req = getRequest();
		String text = 	"Request URI: " + req.getOriginalRef()
						+ "\nMethod: " + req.getMethod()
						+ "\nIP Address: " + getClientInfo().getAddress()
						+ "\nAgent Name: " + getClientInfo().getAgentName()
						+ "\nAgent Version: " + getClientInfo().getAgentVersion()
						+ "\n";
		log.info("message=[" + text.replace("\n", "; ") + "]");
		StringRepresentation resp = new StringRepresentation(text);
		log.trace("function=doPostRequest status=done");
		
		return resp;
	}
	
	@Get
	@Override
	public Representation doGetRequest(Representation entity){
		log.trace("function=doGetRequest status=init");
		
		Request req = getRequest();
		String text = 	"Request URI: " + req.getOriginalRef()
						+ "\nMethod: " + req.getMethod()
						+ "\nIP Address: " + getClientInfo().getAddress()
						+ "\nAgent Name: " + getClientInfo().getAgentName()
						+ "\nAgent Version: " + getClientInfo().getAgentVersion()
						+ "\n";
		log.info("message=[" + text.replace("\n", "; ") + "]");
		StringRepresentation resp = new StringRepresentation(text);
		log.trace("function=doGetRequest status=done");
		
		return resp;
	}
	
	@Put
	@Override
	public Representation doPutRequest(Representation entity) {
		log.trace("function=doPutRequest status=init");
		
		Request req = getRequest();
		String text = 	"Request URI: " + req.getOriginalRef()
						+ "\nMethod: " + req.getMethod()
						+ "\nIP Address: " + getClientInfo().getAddress()
						+ "\nAgent Name: " + getClientInfo().getAgentName()
						+ "\nAgent Version: " + getClientInfo().getAgentVersion()
						+ "\n";
		log.info("message=[" + text.replace("\n", "; ") + "]");
		StringRepresentation resp = new StringRepresentation(text);
		log.trace("function=doPutRequest status=done");
		
		return resp;
	}

	@Patch
	@Override
	public Representation doPatchRequest(Representation entity) {
		log.trace("function=doPatchRequest status=init");
		
		Request req = getRequest();
		String text = 	"Request URI: " + req.getOriginalRef()
						+ "\nMethod: " + req.getMethod()
						+ "\nIP Address: " + getClientInfo().getAddress()
						+ "\nAgent Name: " + getClientInfo().getAgentName()
						+ "\nAgent Version: " + getClientInfo().getAgentVersion()
						+ "\n";
		log.info("message=[" + text.replace("\n", "; ") + "]");
		StringRepresentation resp = new StringRepresentation(text);
		log.trace("function=doPatchRequest status=done");
		
		return resp;
	}

	@Delete
	@Override
	public Representation doDeleteRequest(Representation entity) {
		log.trace("function=doDeleteRequest status=init");
		
		Request req = getRequest();
		String text = 	"Request URI: " + req.getOriginalRef()
						+ "\nMethod: " + req.getMethod()
						+ "\nIP Address: " + getClientInfo().getAddress()
						+ "\nAgent Name: " + getClientInfo().getAgentName()
						+ "\nAgent Version: " + getClientInfo().getAgentVersion()
						+ "\n";
		log.info("message=[" + text.replace("\n", "; ") + "]");
		StringRepresentation resp = new StringRepresentation(text);
		log.trace("function=doDeleteRequest status=done");
		
		return resp;
	}
}