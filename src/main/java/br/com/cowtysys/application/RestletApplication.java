package br.com.cowtysys.application;

import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.resource.Directory;
import org.restlet.routing.VirtualHost;

import br.com.cowtysys.auth.SecurityAuthenticator;
import br.com.cowtysys.controllers.SimpleController;

public class RestletApplication extends Application{
	
	private static final String ROOT_URI = "clap:///br/com/cowtysys/templates/assets";
	private static final String LOCALHOST = "127.0.0.1";
	private static final String PORT = "80";
	private static final String AGGREGATOR = "|";
	
	public RestletApplication(){
		setName("WebService Rest Portable");
		setDescription("WebService portable with Restlet");
		setOwner("CowtySys");
		setAuthor("Efraim Ferreira");
		setStatusService(new ServerStatusService());
	}
	
	@Override
	public synchronized Restlet createInboundRoot() {
		
		VirtualHost router = new VirtualHost(getContext());
		router.setHostDomain(LOCALHOST + AGGREGATOR + Main.getServerAddress());
		router.setHostPort(PORT + AGGREGATOR + Integer.toString(Main.getServerPort()));
		
		//Resources
		router.attach("/test", SimpleController.class);
		
		//Static Files
		router.attach("/assets", getStaticResource(ROOT_URI));
		
		SecurityAuthenticator auth = new SecurityAuthenticator(getContext());
		auth.setNext(router);
		
		return auth;
	}

	private Directory getStaticResource(String uri) {
		Directory dir = new Directory(getContext(), uri);
		dir.setListingAllowed(true);
		dir.setDeeplyAccessible(true);
		dir.setNegotiatingContent(true);
		return dir;
	}
	
}
