package br.com.cowtysys.application;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.service.StatusService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.cowtysys.util.TemplateHelper;
import freemarker.template.TemplateException;

public class ServerStatusService extends StatusService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ServerStatusService.class);
	
	private static final String PAGE = "error";
	
	@Override
	public Representation getRepresentation(Status status, Request request, Response response){
		
		Representation resp = null;
		try{
			Map<String, Object> data = new HashMap<String, Object>();
			data.put("errorCode", status.getCode());
			data.put("message", status.getDescription());
			LOGGER.warn("Status(" + status.getCode() + "): " + status.getDescription() + "[" + request.getOriginalRef() + "]");
			resp = TemplateHelper.loadTemplate(data, PAGE, MediaType.TEXT_HTML);
		}catch(IOException | TemplateException e){
			resp = new StringRepresentation("Error " + status.getCode() + "! " + status.getDescription() + "\n");
		}
		return resp;
	}
}
