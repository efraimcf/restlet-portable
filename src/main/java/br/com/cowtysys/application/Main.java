package br.com.cowtysys.application;

import org.restlet.Component;
import org.restlet.data.Protocol;
import org.restlet.resource.ServerResource;

public class Main extends ServerResource {  

	private static final String SERVER_ADDRESS = "186.207.53.205";
	private static final int SERVER_PORT = 8081;
	
	public static void main(String[] args) throws Exception {  
		// Create the HTTP server
		Component server = new Component();
		
		server.getServers().add(Protocol.HTTP, SERVER_PORT);
		server.getClients().add(Protocol.FILE);
		server.getClients().add(Protocol.CLAP);
		
		server.getDefaultHost().attach(new RestletApplication());
		server.start();
	}
	
	public static String getServerAddress(){
		return SERVER_ADDRESS;
	}
	
	public static int getServerPort(){
		return SERVER_PORT;
	}
}  